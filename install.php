<?php
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}
$sql = <<<EOF
CREATE TABLE IF NOT EXISTS `pre_jsonapi_applist` (
  `appid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appname` varchar(255) DEFAULT '',
  `appkey` varchar(255) DEFAULT '',
  `apilist` varchar(255) DEFAULT '',
  PRIMARY KEY (`appid`)
) ENGINE=MyISAM;
CREATE TABLE IF NOT EXISTS `pre_jsonapi_apilogs` (
  `logid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appid` int(11) unsigned NOT NULL DEFAULT '0',
  `api` varchar(255) DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `dateline` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`logid`)
) ENGINE=MyISAM;
EOF;

runquery($sql);
$finish = TRUE;

?>