<?php 
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once DISCUZ_ROOT.'./source/plugin/jsonapi/functions.php';
jsonApiLoad();

$navtitle='JSON API帮助文档 For Discuz!';
include template('jsonapi:help');
?>