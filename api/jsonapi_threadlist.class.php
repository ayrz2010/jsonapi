<?php 
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
/**
* 新增接口说明
* 1.接口文件放置于\source\plugin\jsonapi\api\目录下，文件名格式为：类名.class.php，其中类名为：jsonapi_api名称，必须严格按要求定义，否则不能被识别；
* 2.类的定义以 jsonapi_threadlist 为模板参考，实现重点：
* 	2.1、静态定义api名称、介绍、字段集合等数据；
*	2.2、类实例化时加载缓存的配置信息，配置信息主要包括接口开启状态、接口返回字段、筛选字段、排序字段等内容，按本例格式处理即可；
*   2.3、实现接口数据返回方法 getData
* 3.由于涉及汉字，接口文件编码必须与当前网站编码方式一致，否则前后台调用的时候容易出现乱码！
* 4.接口调用方法详见：http://你的域名/plugin.php?id=jsonapi:help
* 5.如需定制接口，请联系QQ594941227
*/


class jsonapi_threadlist{
	//API定义名称
	public static $_api='threadlist';
	//API名称，用于显示
	public static $_name='主题列表';
	//API介绍
	public static $_info='获得主题列表的api，可以使用分页方式多次请求！';
	//API配置数据缓存键值，必须唯一
	public static $_cachekey='jsonapi_threadlist_config';
	//API要操作的字段集合
	public static $_keylist=array(
		'tid'=>'主题tid',
		'fid'=>'所在版块fid',
		'typeid'=>'主题分类typeid',
		'sortid'=>'分类信息sortid',
		'readperm'=>'阅读权限',
		'price'=>'主题售价',
		'author'=>'作者用户名',
		'authorid'=>'作者UID',
		'subject'=>'主题标题',
		'dateline'=>'发布时间',
		'lastpost'=>'最后回复时间',
		'lastposter'=>'最后回复人',
		'views'=>'查看数',
		'replies'=>'回复数',
		'displayorder'=>'主题状态',
		'highlight'=>'高亮',
		'digest'=>'精华',
		'recommend_add'=>'支持数',
		'recommend_sub'=>'反对数',
		'favtimes'=>'收藏数',
	
	);
	//配置数据，实例化时加载
	public $_config;
	//定义当前接口单页请求数据，为0则使用插件后台默认设置
	public $_pagenum=20;
	
	//实例化，加载配置信息
	function __construct(){
		global $_G;	
		$this->getConfig();
	}
	
	//加载配置信息
	function getConfig(){
		global $_G;
		loadcache(self::$_cachekey);
		$this->_config=$_G['cache'][self::$_cachekey];
	}
	
	//根据API请求信息，返回相应数据
	function getData($where='',$orderby='',$sort='',$limit=''){
		if(!$this->_config['return']) return array('data'=>array(),'allcount'=>0);
		else{
			$retrun=implode(',',array_keys($this->_config['return']));
			$allcount=DB::result_first("select count(*) from ".DB::table('forum_thread')." where 1 $where ");
			$data=DB::fetch_all("select $retrun from ".DB::table('forum_thread')." where 1 $where $orderby $sort $limit");
			return array('data'=>$data,'allcount'=>$allcount);
		}
	}
}



?>